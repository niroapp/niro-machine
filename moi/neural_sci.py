import matplotlib.pyplot as plt
from matplotlib import cm
import json
import numpy as np
from scipy import stats, optimize
import copy
import random
import openpyxl
from sklearn.neural_network import MLPRegressor

from training_sets import get_training_data


Xtr, ttr = get_training_data()

input_dim = len(Xtr[0])
output_dim = len(ttr[0])

regressors = []

for i in range(output_dim):
    ttri = ttr[:,i] # just train on water

    reg = MLPRegressor(
        hidden_layer_sizes=(100, 50, 25), activation='relu', solver='lbfgs', alpha=0.001, batch_size='auto',
        learning_rate='constant', learning_rate_init=0.01, power_t=0.5, max_iter=10000000, shuffle=True,
        tol=0.0000001)


    reg.fit(Xtr, ttri)

    regressors.append(reg)

sample_index = np.random.randint(0, len(Xtr))  # 40
sample = Xtr[sample_index]
sample_target = ttr[sample_index]

sample_predictions = []

for i in range(output_dim):
    sample_predictions.append(regressors[i].predict(sample.reshape(1, -1))[0])

ticks = np.arange(output_dim)
bar_width = 0.35
plt.bar(ticks + bar_width, sample_target, bar_width, color="r")
plt.bar(ticks, sample_predictions, bar_width, color="b")
#plt.xticks(ticks, label_names)

plt.show()

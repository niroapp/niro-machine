import matplotlib.pyplot as plt
import matplotlib as mpl
import json
import numpy as np
from scipy import stats, optimize
import copy
import random
import openpyxl
from sklearn.metrics import r2_score


with open('outputs/records.json') as f:
    data = json.load(f)


ethanol_volume = 0.15
water_volume = 0.3

mix = []
for record in data:
    if "ethanol" not in record["volumes"]:
        continue

    if record["water"] == 0:
        continue

    if record["volumes"]["ethanol"] != ethanol_volume:
        continue

    if record["water"] != water_volume:
        continue

    mix = record["spectra"]
    break

alone_ethanol = []
for record in data:
    if "ethanol" not in record["volumes"]:
        continue

    if record["water"] > 0:
        continue

    if record["volumes"]["ethanol"] != ethanol_volume:
        continue

    alone_ethanol = record["spectra"]
    break


alone_water = []
for record in data:
    pure = True
    for compound in record["volumes"]:
        if compound != "water":
            pure = False

    if not pure:
        continue

    if record["water"] != water_volume:
        continue

    alone_water = record["spectra"]
    break

estimate_alone_ethanol = np.array(mix) - np.array(alone_water)

x = range(740, 1071)
print(estimate_alone_ethanol.shape)
plt.rc('text', usetex=True)
plt.rc('font', family='serif')
plt.rcParams['figure.figsize'] = 7, 4

color_n = 4
mpl.rcParams['axes.prop_cycle'] = mpl.cycler(color=[plt.get_cmap('Blues')(1. * (i + 2) / (color_n + 2)) for i in range(color_n)])

plt.title("Demonsrtation of additivity with spectra")
plt.xlabel("Moles")
plt.ylabel("Amplitude")

plt.plot(x, estimate_alone_ethanol, label="Difference Ethanol")
plt.plot(x, alone_ethanol, label="Actual Ethanol")
plt.plot(x, alone_water, label="Water")
plt.plot(x, mix, label="Ethanol + Water")

plt.legend()
plt.savefig("./experiment_mixandalone.pdf")
plt.show()

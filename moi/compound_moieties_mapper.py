import matplotlib.pyplot as plt
from matplotlib import cm
import openpyxl
import json
import numpy as np
from scipy import stats, optimize
import copy
import random

SUFFIX = "complex"

workbook = openpyxl.load_workbook(f'references/SCiO database {SUFFIX}.xlsx', data_only=True, read_only=True)
moieties_worksheet = workbook['moieties']

dataset = {}
label_names = []
for i in range(3, moieties_worksheet.max_row):
    name = moieties_worksheet.cell(row=i, column=1).value

    if name is not None:
        name = name.lower()

        obj = {}
        try:
            obj["rmm"] = float(moieties_worksheet.cell(row=i, column=5).value)
            obj["density"] = float(moieties_worksheet.cell(row=i, column=6).value)
        except:
            continue

        values = {}
        for f in range(8, moieties_worksheet.max_column):
            moitie_name = moieties_worksheet.cell(row=2, column=f).value

            if moitie_name is not None:
                value = moieties_worksheet.cell(row=i, column=f).value
                if value is None:
                    value = 0

                values[moitie_name] = float(value)

        obj["values"] = values

        dataset[name] = obj

    print(name)

with open(f'outputs/compound_moieties_{SUFFIX}.json', 'w') as f:
    json.dump(dataset, f)

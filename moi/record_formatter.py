import json
import matplotlib.pyplot as plt
import numpy as np

# Load the json file from SCiO
with open('references/collection_id-87602aed-eb25-4221-a107-bf6b62cd5289-3751503312340592651.json') as f:
    data = json.load(f)

# Generate a baseline of the apparatus
base_lines = {}
base_lines_count = {}

base_line = np.zeros(len(data["records"][0]["sample_raw"]))
base_line_count = 0

for record in data["records"]:
    if record["Name"] in ["3-empty-vials", "3 empty vials"]:
        date = record["batch"]
        spectra = np.array([float(x) for x in record["sample_raw"]])

        if date not in base_lines:
            base_lines[date] = spectra
            base_lines_count[date] = 1
        else:
            base_lines[date] += spectra
            base_lines_count[date] += 1

        base_line += spectra
        base_line_count += 1

for key in base_lines.keys():
    base_lines[key] /= base_lines_count[key]

base_line /= base_line_count

dataset = []

for record in data["records"]:
    name = record["Name"].lower().replace(" mix", "")

    spectra = None

    if record["batch"] not in base_lines:
        spectra = np.log(base_line / np.array([float(x) for x in record["sample_raw"]]))
    else:
        spectra = np.log(base_lines[record["batch"]] / np.array([float(x) for x in record["sample_raw"]]))

    volumes = {}
    masses = {}
    water = 0

    if record["Sample Vol"] != "n/a" and float(record["Sample Vol"]) != 0:
        volumes[name] = float(record["Sample Vol"])

    if record["Sample mass"] != "n/a" and float(record["Sample mass"]) != 0:
        masses[name] = float(record["Sample mass"])

    if record["Water or SDS vol"] != "n/a" and float(record["Water or SDS vol"]) != 0:
        water = float(record["Water or SDS vol"])

    dataset.append({"water": water, "volumes": volumes, "masses": masses, "spectra": spectra.tolist()})


with open('outputs/records.json', 'w') as f:
    json.dump(dataset, f)

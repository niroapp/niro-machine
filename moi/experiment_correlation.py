import matplotlib.pyplot as plt
import matplotlib as mpl
import json
import numpy as np
from scipy import stats, optimize
import copy
import random
import openpyxl
from sklearn.metrics import r2_score


with open('outputs/records.json') as f:
    data = json.load(f)

x = []
y = []
for record in data:
    water = record["water"]
    if water > 1:
        continue

    if "water" in record["volumes"]:
        water += record["volumes"]["water"]
        #print(record["volumes"]["water"])

    if water > 0:
        x.append(water)
        y.append(record["spectra"][233])

plt.rc('text', usetex=True)
plt.rc('font', family='serif')
plt.rcParams['figure.figsize'] = 7, 4

color_n = 4
mpl.rcParams['axes.prop_cycle'] = mpl.cycler(color=[plt.get_cmap('Blues')(1. * (i + 5) / (color_n + 1)) for i in range(color_n)])

plt.title("Water Absorption at 975nm")
plt.xlabel("Moles")
plt.ylabel("Amplitude")

plt.scatter(x, y, marker=".")
plt.plot(np.unique(x), np.poly1d(np.polyfit(x, y, 1))(np.unique(x)), c="k", label="Best Fit")

plt.legend()
plt.savefig("./experiment_correlation.pdf")
plt.show()

print(np.corrcoef(x, y))

import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.ticker import FormatStrFormatter
import json
import numpy as np
from scipy import stats, optimize
import copy
import random
import openpyxl
from sklearn.metrics import r2_score
import matplotlib.pyplot as plt
import matplotlib as mpl

# using errors to give importance, means we can use 0 substance to act as null hypothesis
# nice water graph, shows not to give importance to exactly where the peak is (where the intersect is)


class Model:
    def __init__(self, label_names, points, targets):
        self.label_names = label_names

        self.coefficients = []
        self.errors = []

        for n in range(len(points[0])):
            xtr = []
            ttr = []

            for f in range(len(points)):
                xtr.append(targets[f])
                ttr.append(points[f][n])

            xtr = np.array(xtr)
            ttr = np.array(ttr)

            coefs, resid = np.linalg.lstsq(xtr, ttr, rcond=None)[:2]

            self.coefficients.append(coefs)

            if len(resid) == 1:
                self.errors.append(resid[0])
            else:
                self.errors.append(1e-10)

        self.errors = np.array(self.errors)
        self.errors = np.sqrt(self.errors ** 2)

    def generate_spectra(self, x, *argv):
        y = [0] * len(x)

        for i in range(len(x)):
            y[i] = argv @ self.coefficients[i]

        return y

    def predict(self, sample):
        x = np.zeros(len(sample))
        y = sample
        popt, pcov = optimize.curve_fit(self.generate_spectra, x, y, p0=[0] * len(self.label_names), bounds=(0, np.Inf), sigma=self.errors, absolute_sigma=True)

        return popt


SUFFIX = "complex"

with open('outputs/records.json') as f:
    data = json.load(f)

with open(f'outputs/compound_moieties_{SUFFIX}.json') as f:
    data_moieties = json.load(f)

workbook = openpyxl.load_workbook(f'references/SCiO database {SUFFIX}.xlsx', data_only=True, read_only=True)
moieties_worksheet = workbook['moieties']

label_names = []

for f in range(8, moieties_worksheet.max_column):
    label_names.append(moieties_worksheet.cell(row=2, column=f).value)


""" # progrssing through
all_allowed_substances = [
    'water', 'ethanol', 'acetone', 'citronellol', 'dipentene', 'carvone',
    'toluene', 'linalool', 'diisobutyl-ketone', 'tridec-1-ene', 'geraniol',
    'eugenol', 'methanol', 'nerolidol', 'heptanal', 'cyclohexanethiol', 'diphenyl-ether',
    'acetonitrile', 'methyl-acetate', 'cyclohexanone-diethylketal', '2-butyne',
    'ethylene-glycol', 'benzaldehyde', 'i-pentyl-acetate', 'cyclohexeneoxide',
    '1,4-cyclohexadiene', 'propane-1-2-diol', 'furan', 't-butanol', 't-butyl-methyl-ether',
    'alpha-angelica-lactone'
]


total_differences = []
for i in range(2, len(all_allowed_substances)):
    allowed_substances = all_allowed_substances[:i]
    print(allowed_substances)

    targets = []
    inputs = []
    for record in data:
        found = False
        for allowed in allowed_substances:
            if allowed in record['volumes']:
                found = True

        if not found:
            continue

        # We're only looking at liquids in water
        if record['water'] == 0 and ("HOH" not in record['volumes'] or record['volumes']["HOH"] == 0):
            continue

        # Ignore huge valued records
        if record['water'] > 1 or [x for x in record["volumes"] if record["volumes"][x] > 1]:
            continue

        unknown_compound = False

        target = [0] * len(label_names)

        # Given equation for the water value
        index = label_names.index("HOH")
        target[index] += 2.0 * (float(record['water']) / 18.0)


        for compound in record['volumes']:
            if compound not in data_moieties:
                unknown_compound = True
                break

            for moiety in data_moieties[compound]["values"]:
                if moiety not in label_names:
                    unknown_compound = True
                    break

                index = label_names.index(moiety)
                target[index] += (float(data_moieties[compound]["values"][moiety]) / float(data_moieties[compound]["rmm"])) * ((float(record['volumes'][compound]) * float(data_moieties[compound]["density"])) / 1000.0)

        for compound in record['masses']:
            if compound not in data_moieties:
                unknown_compound = True
                break

            for moiety in data_moieties[compound]["values"]:
                if moiety not in label_names:
                    unknown_compound = True
                    break

                index = label_names.index(moiety)
                target[index] += (float(data_moieties[compound]["values"][moiety]) / float(data_moieties[compound]["rmm"])) * ((float(record['masses'][compound])) / 1000.0)

        if unknown_compound:
            continue

        sub_spectra = np.array(record['spectra'])[100:300]
        inputs.append(np.concatenate((np.gradient(np.gradient(sub_spectra)), np.gradient(sub_spectra), sub_spectra)))

        targets.append(target)

    targets = np.array(targets)

    empty_moities = np.where(~targets.any(axis=0))[0]

    print(targets.shape)
    sub_targets = np.delete(targets, empty_moities, axis=1)
    sub_label_names = np.delete(label_names, empty_moities, axis=0)
    print(targets.shape)

    model = Model(sub_label_names, inputs, sub_targets)

    total_difference = 0
    print("SAMPLES:", len(inputs))
    for sample_index in range(len(inputs)):
        print(f"SAMPLE INDEX: {sample_index}")
        sample = inputs[sample_index]
        sample_target = sub_targets[sample_index]

        popt = model.predict(sample)

        total_difference += np.sum(np.abs(sample_target - popt))

    average_total_difference = total_difference / len(inputs)
    total_differences.append(average_total_difference)

    print(total_differences)

"""

"""
fig, ax1 = plt.subplots()
ax1.plot(errors, color="k")

ax2 = ax1.twinx()
ax2.plot(r2, color="b")

plt.show()
"""


"""
sample_index = np.random.randint(0, len(inputs))  # 40
sample = inputs[sample_index]
sample_target = targets[sample_index]

x = np.zeros(len(sample))
y = sample

popt, pcov = optimize.curve_fit(generate_spectra, x, y, p0=[0] * len(label_names), bounds=(0, np.Inf), sigma=errors, absolute_sigma=True)
#popt, pcov = optimize.curve_fit(generate_spectra, x, y, p0=[0] * len(label_names), bounds=(0, np.Inf))

for i in [0, 6]:
    just_water = [0] * len(label_names)
    just_water[i] = 1
    just_water = np.array(just_water)

    plt.ylabel("Absorption $1^{st}$ derivative")
    plt.xlabel("Wavelength (nm)")

    plt.plot(range(740, 1071), generate_spectra(x, just_water), label=label_names[i])

plt.legend()
plt.show()
"""

# Unit mapping
allowed_substances = [
    'water', 'ethanol', 'acetone', 'citronellol', 'dipentene', 'carvone',
    'toluene', 'linalool', 'diisobutyl-ketone', 'tridec-1-ene', 'geraniol',
    'eugenol', 'methanol', 'nerolidol', 'heptanal', 'cyclohexanethiol', 'diphenyl-ether',
    'acetonitrile', 'methyl-acetate', 'cyclohexanone-diethylketal', '2-butyne',
    'ethylene-glycol', 'benzaldehyde', 'i-pentyl-acetate', 'cyclohexeneoxide',
    '1,4-cyclohexadiene', 'propane-1-2-diol', 'furan', 't-butanol', 't-butyl-methyl-ether',
    'alpha-angelica-lactone'
]
targets = []
inputs = []
for record in data:
    found = False
    for allowed in allowed_substances:
        if allowed in record['volumes']:
            found = True

    if not found:
        continue

    # We're only looking at liquids in water
    #if record['water'] == 0 and ("HOH" not in record['volumes'] or record['volumes']["HOH"] == 0):
    #    continue

    # Ignore huge valued records
    if record['water'] > 1 or [x for x in record["volumes"] if record["volumes"][x] > 1]:
        continue

    unknown_compound = False

    target = [0] * len(label_names)

    # Given equation for the water value
    index = label_names.index("HOH")
    target[index] += 2.0 * (float(record['water']) / 18.0)


    for compound in record['volumes']:
        if compound not in data_moieties:
            unknown_compound = True
            break

        for moiety in data_moieties[compound]["values"]:
            if moiety not in label_names:
                unknown_compound = True
                break

            index = label_names.index(moiety)
            target[index] += (float(data_moieties[compound]["values"][moiety]) / float(data_moieties[compound]["rmm"])) * ((float(record['volumes'][compound]) * float(data_moieties[compound]["density"])) / 1000.0)

    for compound in record['masses']:
        if compound not in data_moieties:
            unknown_compound = True
            break

        for moiety in data_moieties[compound]["values"]:
            if moiety not in label_names:
                unknown_compound = True
                break

            index = label_names.index(moiety)
            target[index] += (float(data_moieties[compound]["values"][moiety]) / float(data_moieties[compound]["rmm"])) * ((float(record['masses'][compound])) / 1000.0)

    if unknown_compound:
        continue

    sub_spectra = np.array(record['spectra'])[100:300]
    inputs.append(np.concatenate((np.gradient(np.gradient(sub_spectra)), np.gradient(sub_spectra), sub_spectra)))

    targets.append(target)

targets = np.array(targets)

empty_moities = np.where(~targets.any(axis=0))[0]

print(targets.shape)
targets = np.delete(targets, empty_moities, axis=1)
label_names = np.delete(label_names, empty_moities, axis=0)
print(targets.shape)

model = Model(label_names, inputs, targets)
"""

# Showing the plots nicely
sample_index = np.random.randint(0, len(inputs))  # 40
print(f"SAMPLE INDEX: {sample_index}")
sample = inputs[sample_index]
sample_target = targets[sample_index]


popt = model.predict(sample)
print(sample_target)
print(popt)

x = np.zeros(len(sample))
generated_spectra = model.generate_spectra(x, popt)
perfect_spectra = model.generate_spectra(x, sample_target)

fig, axes = plt.subplots(3, 1)
plt.tight_layout()

ax1 = axes[0]
start = 2 * 200
end = start + 200
ax1.set_title("absorption")
#ax1.plot(errors[start:end], color="k")
ax2 = ax1.twinx()
ax2.plot(sample[start:end], color="r")
ax2.plot(generated_spectra[start:end], color="b")
ax2.plot(perfect_spectra[start:end], color="g")

ax1 = axes[1]
start = 1 * 200
end = start + 200
ax1.set_title("$1^{st}$ derivative")
#ax1.plot(errors[start:end], color="k")
ax2 = ax1.twinx()
ax2.plot(sample[start:end], color="r")
ax2.plot(generated_spectra[start:end], color="b")
ax2.plot(perfect_spectra[start:end], color="g")

ax1 = axes[2]
start = 0 * 200
end = start + 200
ax1.set_title("$2^{nd}$ derivative")
#ax1.plot(errors[start:end], color="k")
ax2 = ax1.twinx()
ax2.plot(sample[start:end], color="r")
ax2.plot(generated_spectra[start:end], color="b")
ax2.plot(perfect_spectra[start:end], color="g")

plt.show()

ticks = np.arange(len(label_names))
bar_width = 0.35
plt.bar(ticks + bar_width, sample_target, bar_width, color="r")
#plt.bar(label_names, popt, bar_width, yerr=np.sqrt(np.diag(pcov)), color="b")
plt.bar(label_names, popt, bar_width, color="b")
plt.xticks(ticks, label_names)

plt.show()
"""

#making units
wavelength_range = range(840, 1040)

for i in range(len(label_names)):
    x = inputs[0]
    popt = np.zeros(len(label_names))

    popt[i] = 1

    generated_spectra = model.generate_spectra(x, popt)


    plt.rc('text', usetex=True)
    plt.rc('font', family='serif')
    plt.rcParams['figure.figsize'] = 7, 4

    fig, axes = plt.subplots(3, 1)

    fig.set_size_inches(5, 6.5)

    fig.tight_layout()

    ax1 = axes[0]
    start = 2 * 200
    end = start + 200
    ax1.set_title("Absorption")
    ax1.yaxis.set_major_formatter(FormatStrFormatter('%.2f'))
    ax1.set_xlabel("Wavelength")
    ax1.set_ylabel("Amplitude")
    ax1.plot(wavelength_range, generated_spectra[start:end], c="#0c296d")

    ax1 = axes[1]
    start = 1 * 200
    end = start + 200
    ax1.set_title("$1^{st}$ derivative")
    ax1.yaxis.set_major_formatter(FormatStrFormatter('%.2f'))
    ax1.set_xlabel("Wavelength")
    ax1.set_ylabel("Amplitude")
    ax1.plot(wavelength_range, generated_spectra[start:end], c="#0c296d")

    ax1 = axes[2]
    start = 0 * 200
    end = start + 200
    ax1.set_title("$2^{nd}$ derivative")
    ax1.yaxis.set_major_formatter(FormatStrFormatter('%.2f'))
    ax1.set_xlabel("Wavelength")
    ax1.set_ylabel("Amplitude")
    ax1.plot(wavelength_range, generated_spectra[start:end], c="#0c296d")

    fig.tight_layout()
    fig.subplots_adjust(top=0.9)
    plt.savefig(f'./outputs/img/{SUFFIX}/{label_names[i].replace("/", ",")}.pdf')
    plt.close()


""" confidence of different datasets
# simple - 0.34726820714381823
# complex - 0.378168984513989
# merge - 0.3410033014663918
dataset = {"data": []}

for i in range(len(inputs)):
    x = np.zeros(len(inputs[i]))
    y = inputs[i]
    popt, pcov = optimize.curve_fit(generate_spectra, x, y, p0=[0] * len(label_names), bounds=(0, np.Inf), sigma=errors, absolute_sigma=True)

    average_variance = np.sum(np.sqrt(np.diag(pcov)))
    print(f"({i}/{len(inputs)}): {average_variance}")

    dataset["data"].append(average_variance)

with open(f'outputs/results_{SUFFIX}.json', 'w') as f:
    json.dump(dataset, f)
"""


""" absolute differences
# complex - 80.12745550164043 0.09241921049785516
average_differences = []
for i in range(len(inputs)):
    x = np.zeros(len(inputs[i]))
    y = inputs[i]
    popt, pcov = optimize.curve_fit(generate_spectra, x, y, p0=[0] * len(label_names), bounds=(0, np.Inf), sigma=errors, absolute_sigma=True)

    #print(r2_score(popt, targets[i]))
    average_difference = np.sum(np.abs(popt - targets[i])) / len(targets[i])
    print(f"({i}/{len(inputs)}): {average_difference}")

    average_differences.append(average_difference)

print(f"sum average differences: {sum(average_differences)}")
print(f"mean average differences: {sum(average_differences) / len(average_differences)}")
"""

"""
# TODO: we can transpose and do in one
for i in range(len(targets[0])):  # test
    errors = []
    for n in range(len(inputs[0])):  # test
        xtr = []
        ttr = []

        xte = []
        tte = []
        for f in range(len(inputs)):
            if targets[f][i] > 0:  # Don't like mess at 0... maybe take out?
                xtr.append(inputs[f][n])
                ttr.append(targets[f][i])

                # optional allow all data to be tested on?..
                xte.append(inputs[f][n])
                tte.append(targets[f][i])

        xtr = np.array(xtr)
        ttr = np.array(ttr)

        xte = np.array(xte)
        tte = np.array(tte)

        # We can't predict this moitie
        if len(xtr) == 0:
            break

        coefs, resid = np.linalg.lstsq(np.transpose([xtr]), ttr, rcond=None)[:2]

        pred = xte * coefs

        errors.append(r2_score(tte, pred))

    # We can't predict this moitie
    if len(errors) == 0:
        print(f"{label_names[i]} has no inputs")
        continue

    errors = np.array(errors)

    plt.title(f"{label_names[i]}")
    plt.plot(errors)
    plt.show()
"""

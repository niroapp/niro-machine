The below commands detail how to get tensorflow working on *my* 
arch distro:

```
yaourt -S python36
python3.6 -m venv venv
source ./venv/bin/activate
pip install -r ./requirements.txt
```

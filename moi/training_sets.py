import matplotlib.pyplot as plt
from matplotlib import cm
import json
import numpy as np
from scipy import stats, optimize
import copy
import random
import openpyxl
from sklearn.metrics import r2_score


def get_training_data():
    with open('outputs/records.json') as f:
        data = json.load(f)

    with open('outputs/compound_moieties.json') as f:
        data_moieties = json.load(f)

    workbook = openpyxl.load_workbook('references/SCiO database v3.xlsx', data_only=True, read_only=True)
    moieties_worksheet = workbook['moieties']

    label_names = []

    for f in range(8, moieties_worksheet.max_column):
        label_names.append(moieties_worksheet.cell(row=2, column=f).value)

    inputs = []
    targets = []
    for record in data:
        # We're only looking at compounds in water
        if "water" not in record['volumes'] or len(record['volumes']) == 1:
            continue

        unknown_compound = False

        target = [0] * len(label_names)
        for compound in record['volumes']:
            if compound not in data_moieties:
                unknown_compound = True
                break

            for moiety in data_moieties[compound]:
                if moiety not in label_names:
                    unknown_compound = True
                    break


                index = label_names.index(moiety)
                target[index] += data_moieties[compound][moiety] * record['volumes'][compound]

        if unknown_compound:
            continue

        #inputs.append(np.gradient(record['spectra'][120:240]))
        inputs.append(np.gradient(record['spectra']))
        #inputs.append(record['spectra'])

        targets.append(target)

    targets = np.array(targets)

    empty_moities = np.where(~targets.any(axis=0))[0]

    targets = np.delete(targets, empty_moities, axis=1)
    # label_names = np.delete(label_names, empty_moities, axis=0)
    return inputs, targets

import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.ticker import FormatStrFormatter
import json
import numpy as np
from scipy import stats, optimize
import copy
import random
import openpyxl
from sklearn.metrics import r2_score

# using errors to give importance, means we can use 0 substance to act as null hypothesis
# nice water graph, shows not to give importance to exactly where the peak is (where the intersect is)


class Model:
    def __init__(self, label_names, points, targets):
        self.label_names = label_names

        self.coefficients = []
        self.errors = []

        for n in range(len(points[0])):
            xtr = []
            ttr = []

            for f in range(len(points)):
                xtr.append(targets[f])
                ttr.append(points[f][n])

            xtr = np.array(xtr)
            ttr = np.array(ttr)

            coefs, resid = np.linalg.lstsq(xtr, ttr, rcond=None)[:2]

            self.coefficients.append(coefs)

            if len(resid) == 1:
                self.errors.append(resid[0])
            else:
                self.errors.append(1e-10)

        self.errors = np.array(self.errors)
        self.errors = np.sqrt(self.errors ** 2)

    def generate_spectra(self, x, *argv):
        y = [0] * len(x)

        for i in range(len(x)):
            y[i] = argv @ self.coefficients[i]

        return y

    def predict(self, sample):
        x = np.zeros(len(sample))
        y = sample
        popt, pcov = optimize.curve_fit(self.generate_spectra, x, y, p0=[0] * len(self.label_names), bounds=(0, np.Inf), sigma=self.errors, absolute_sigma=True)

        return popt


SUFFIX = "complex"

with open('outputs/records.json') as f:
    data = json.load(f)

with open(f'outputs/compound_moieties_{SUFFIX}.json') as f:
    data_moieties = json.load(f)

workbook = openpyxl.load_workbook(f'references/SCiO database {SUFFIX}.xlsx', data_only=True, read_only=True)
moieties_worksheet = workbook['moieties']


moities_label_names = []

for f in range(8, moieties_worksheet.max_column):
    moities_label_names.append(moieties_worksheet.cell(row=2, column=f).value)


all_allowed_substances = [
    'water', 'ethanol', 'acetone', 'citronellol', 'dipentene', 'carvone',
    'toluene', 'linalool', 'diisobutyl-ketone', 'tridec-1-ene', 'geraniol',
    'eugenol', 'methanol', 'nerolidol', 'heptanal', 'cyclohexanethiol', 'diphenyl-ether',
    'acetonitrile', 'methyl-acetate', 'cyclohexanone-diethylketal', '2-butyne',
    'ethylene-glycol', 'benzaldehyde', 'i-pentyl-acetate', 'cyclohexeneoxide',
    '1,4-cyclohexadiene', 'propane-1-2-diol', 'furan', 't-butanol', 't-butyl-methyl-ether',
    'alpha-angelica-lactone'
]

total_differences = []
for i in range(2, len(all_allowed_substances)):
    allowed_substances = all_allowed_substances[:i]

    print(allowed_substances)
    label_names = allowed_substances
    inputs = []
    targets = []

    for record in data:
        found = False
        for allowed in allowed_substances:
            if allowed in record['volumes']:
                found = True

        if not found:
            continue

        # We're only looking at liquids in water
        if record['water'] == 0 and ("HOH" not in record['volumes'] or record['volumes']["HOH"] == 0):
            continue

        # Ignore huge valued records
        if record['water'] > 1 or [x for x in record["volumes"] if record["volumes"][x] > 1]:
            continue

        target = [0] * len(label_names)

        # Given equation for the water value
        index = label_names.index("water")
        target[index] += float(record['water'])

        for compound in record['volumes']:
            index = label_names.index(compound)
            target[index] += float(record['volumes'][compound])

        sub_spectra = np.array(record['spectra'])[100:300]
        inputs.append(np.concatenate((np.gradient(np.gradient(sub_spectra)), np.gradient(sub_spectra), sub_spectra)))

        targets.append(target)

    targets = np.array(targets)

    empty_moities = np.where(~targets.any(axis=0))[0]

    print(targets.shape)
    sub_targets = np.delete(targets, empty_moities, axis=1)
    sub_label_names = np.delete(label_names, empty_moities, axis=0)
    print(targets.shape)

    model = Model(sub_label_names, inputs, sub_targets)

    total_difference = 0
    print("SAMPLES:", len(inputs))
    for sample_index in range(len(inputs)):
        print(f"SAMPLE INDEX: {sample_index}")
        sample = inputs[sample_index]
        sample_target = sub_targets[sample_index]

        popt = model.predict(sample)



        target_moities = [0] * len(moities_label_names)

        # Given equation for the water value
        index = moities_label_names.index("HOH")
        target_moities[index] += 2.0 * (float(sample_target[0]) / 18.0)

        # For each compound we are looking at
        for compound in label_names[1:]:
            # Go through the moieties witin that compound
            for moiety in data_moieties[compound]["values"]:
                # fetch the index of where that moitie is within the label names
                index = moities_label_names.index(moiety)

                # Add the amount of moities of that compound
                target_moities[index] += (float(data_moieties[compound]["values"][moiety]) / float(data_moieties[compound]["rmm"])) * ((float(sample_target[label_names.index(compound)]) * float(data_moieties[compound]["density"])) / 1000.0)


        pred_moities = [0] * len(moities_label_names)

        # Given equation for the water value
        index = moities_label_names.index("HOH")
        pred_moities[index] += 2.0 * (float(popt[0]) / 18.0)

        for compound in label_names[1:]:
            for moiety in data_moieties[compound]["values"]:
                index = moities_label_names.index(moiety)
                pred_moities[index] += (float(data_moieties[compound]["values"][moiety]) / float(data_moieties[compound]["rmm"])) * ((float(popt[label_names.index(compound)]) * float(data_moieties[compound]["density"])) / 1000.0)

        total_difference += np.sum(np.abs(np.array(target_moities) - np.array(pred_moities)))

    average_total_difference = total_difference / len(inputs)
    total_differences.append(average_total_difference)

    print(total_differences)

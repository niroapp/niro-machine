import matplotlib.pyplot as plt
from matplotlib import cm
import json
import numpy as np
from scipy import stats, optimize
import copy
import random

# cyclohexanethiol and heptanal are really similar.
# heptanol 0.1 = cyclohexanethiol 0.046 + heptanol 0.039
# diphenyl-ether = benzaldehyde
# linalool = acetone
# dipentene = methyl-acetate


# interesting interaction, seems to be around the same number, multiple
# wavelengths change the shape, not the overall number, suggests it is not addative.

with open('substances.json') as f:
    data = json.load(f)

with open('substances_moieties.json') as f:
    data_moieties = json.load(f)

for record in data:
    # record['spectra'] = np.gradient(record['spectra'])
    record['spectra'] = np.gradient(record['spectra'][120:240])
    # record['spectra'] = np.array(record['spectra'])

spectra_size = len(data[0]['spectra'])

data_preserved = copy.deepcopy(data)

# Sum of all r^2
importance_data = [0] * spectra_size

substance_models = {}


def find_random_substance(name):
    records = []
    for record in data_preserved:
        if name in record["volumes"]:
            records.append(record)

    return random.choice(records)


def generate_model(volumes, spectra):
    model = []

    for i in range(spectra_size):
        x = []
        y = []

        for f in range(len(volumes)):
            x.append(volumes[f])
            y.append(spectra[f][i])

        x = np.array(x)
        y = np.array(y)

        coefs, resid = np.linalg.lstsq(np.transpose([x]), y, rcond=None)[:2]
        r2 = 1 - resid / (y.size * y.var())

        importance_data[i] += abs(r2)
        model.append(coefs[0])

    return model


def generate_spectra(volumes):
    y = [0] * spectra_size

    for name in volumes:
        if name not in substance_models:
            continue

        volume = volumes[name]

        for i in range(spectra_size):
            y[i] += substance_models[name][i] * volume

    #for i in range(spectra_size):
    #    y[i] /= len(volumes)

    return y


def reduce_substance(name):
    for record in data:
        if name not in record['volumes']:
            continue

        removed_substances = []
        for additive in record['volumes']:
            if additive == name:
                continue

            if additive not in substance_models:
                continue

            record['spectra'] -= generate_spectra({additive: record['volumes'][additive]})
            removed_substances.append(additive)

        for additive in removed_substances:
            del record['volumes'][additive]


def add_substance(name):
    reduce_substance(name)

    volumes = []
    spectra = []
    for i in range(len(data)):
        # allow through base water only! And then only isolated substances that had been in water before.
        # We're focusing on substances suspended in water
        if (len(data_preserved[i]['volumes']) == 1 and 'water' not in data_preserved[i]['volumes']) or len(data[i]['volumes']) != 1 or name not in data[i]['volumes']:
        # if len(data[i]['volumes']) != 1 or name not in data[i]['volumes']:
            continue

        volumes.append(data[i]['volumes'][name])
        spectra.append(data[i]['spectra'])

    if len(volumes) > 0:
        substance_models[name] = generate_model(volumes, spectra)


# Define list to be used
"""
substance_names = [
    'water', 'ethanol', 'acetone', 'citronellol', 'dipentene', 'carvone',
    'toluene', 'linalool', 'diisobutyl-ketone', 'tridec-1-ene', 'geraniol',
    'eugenol', 'methanol', 'nerolidol', 'heptanal', 'cyclohexanethiol', 'diphenyl-ether',
    'acetonitrile', 'methyl-acetate', 'cyclohexanone-diethylketal', '2-butyne',
    'ethylene-glycol', 'benzaldehyde', 'i-pentyl-acetate', 'cyclohexeneoxide',
    '1,4-cyclohexadiene', 'propane-1-2-diol', 'furan', 't-butanol', 't-butyl-methyl-ether',
    'alpha-angelica-lactone'
]
"""

substance_names = ['water']

for i in range(len(data)):
    if 'zz' not in "".join(data[i]['volumes'].keys()) and len(data[i]['volumes']) == 2 and 'water' in data[i]['volumes']:
        substance_names += data[i]['volumes'].keys()

substance_names = list(set(substance_names))
substance_names.insert(0, substance_names.pop(substance_names.index('water')))

substance_names.pop(substance_names.index('furan'))

for name in substance_names:
    add_substance(name)

# plot the sum of r^2 values, to see where on the spectra good correlations are found
#plt.plot(importance_data)
#plt.show()


def combinanator(x, *argv):
    volumes = {}
    for i in range(len(substance_names)):
        volumes[substance_names[i]] = argv[i]
    return generate_spectra(volumes)


unknown_sample = find_random_substance('acetone')

x = list(range(spectra_size))
y = unknown_sample['spectra']

popt, pcov = optimize.curve_fit(combinanator, x, y, p0=[0] * len(substance_names), bounds=(0, np.Inf))

print("Actual volumes:")
actual_substances = sorted(unknown_sample['volumes'], key=unknown_sample['volumes'].get, reverse=True)
for name in actual_substances:
    print(f"{name:<30} {unknown_sample['volumes'][name]:>5.3f}")

print()

print("Predicted volumes:")
predicted_substaces = sorted(range(len(popt)), key=lambda k: popt[k], reverse=True)
for i in predicted_substaces:
    if popt[i] < 0.001:
        break

    print(f"{substance_names[i]:<30} {popt[i]:>5.3f}")

print()

actual_moieties = {}

for name in actual_substances:
    if name not in data_moieties:  # Means not in xlsx, boo!
        print(f"warning - we don't know about {name}")
        continue

    for moiety in data_moieties[name]:
        if moiety in actual_moieties:
            actual_moieties[moiety] += data_moieties[name][moiety] * unknown_sample['volumes'][name]
        else:
            actual_moieties[moiety] = data_moieties[name][moiety] * unknown_sample['volumes'][name]

predicted_moieties = {}

for i in predicted_substaces:
    if name not in data_moieties:  # Means not in xlsx, boo!
        print(f"warning - we don't know about {name}")
        continue

    if substance_names[i] in data_moieties:
        for moiety in data_moieties[substance_names[i]]:
            if moiety in predicted_moieties:
                predicted_moieties[moiety] += data_moieties[substance_names[i]][moiety] * popt[i]
            else:
                predicted_moieties[moiety] = data_moieties[substance_names[i]][moiety] * popt[i]

total_moieties = {k: predicted_moieties.get(k, 0) + actual_moieties.get(k, 0) for k in set(predicted_moieties) | set(actual_moieties)}

xs = []
ys = []
zs = []

total_moieties_names = sorted(total_moieties, key=total_moieties.get, reverse=True)
for name in total_moieties_names:
    if total_moieties[name] < 0.001:
        break

    xs.append(name)

for name in xs:
    if name in actual_moieties:
        ys.append(actual_moieties[name])
    else:
        ys.append(0)

    if name in predicted_moieties:
        zs.append(predicted_moieties[name])
    else:
        zs.append(0)

"""
predicted_moieties_names = sorted(predicted_moieties, key=predicted_moieties.get, reverse=True)
for name in predicted_moieties_names:
    print(f"{name:<30} {predicted_moieties[name]:>5.3f}")

    if predicted_moieties[name] < 0.001:
        break
"""

plt.bar(xs, ys, color='r', width=0.2, align='edge', label='Actual')
plt.bar(xs, zs, color='b', width=-0.2, align='edge', label='Predicted')
plt.xticks(xs)

plt.legend()
plt.show()

t_x = np.linspace(x[0], x[-1], len(x))
t_y = combinanator(t_x, *popt)

plt.plot(x, y, label='Real')
plt.plot(t_x, t_y, label='Estimate')

# for i in range(len(substance_names)):
for i in predicted_substaces:
    plt.plot(generate_spectra({substance_names[i]: popt[i]}), label=f'Estimate ({substance_names[i]})')

    if popt[i] < 0.001:
        break

plt.legend()
plt.show()

import matplotlib.pyplot as plt
from matplotlib import cm
import json
import numpy as np
from scipy import stats
import scipy.optimize as optimize

# For molecules in the liquid state or in solution, collision and proximity broadening predominate and lines are much broader than lines from the same molecule in the gas phase.[8][9] Line maxima may also be shifted. Because there are many sources of broadening, the lines have a stable distribution, tending towards a Gaussian shape.

with open('labeled.json') as f:
    data = json.load(f)

n = 0
record = None
for i in range(len(data)):
    if data[i]["labels"]["HOH"] == 0:
        continue
    if n > 0:
        n -= 1
        continue
    record = data[i]
    break

# record = data[47]
# record = data[67]
record = data[47]
print(record["labels"])
print(i)


# HOH - 235
# CH2- 185
# CH3 - 166
# COH - 271
# COC - 132
# peaks = [235, 185, 168, 153, 271, 132]
# peak_guess = ["HOH", "?", "?", "?", "?", "?"]

peaks = [235, 153, 168]
deviation = [30, 7, 7]
peak_guess = ["HOH"] + (["?"] * 3)


def line(x, *argv):
    #return (argv[0] * x) + argv[1]
    return (x * 0) + argv[0]


def gauss(x, mu, sigma, magnitude):
    return magnitude * ((1 / (np.sqrt(2 * np.pi * np.power(sigma, 2)))) *
           (np.power(np.e, -(np.power((x - mu), 2) / (2 * np.power(sigma, 2))))))


def func(x, *argv):
    result = 0
    for n in range(len(peaks)):
        result += gauss(x, peaks[n], deviation[n], argv[n])

    return result + line(x, argv[-1])


x = np.array(range(len(record["spectra"])))
y = np.array(record["spectra"])


peak_limits = np.take(record["spectra"], peaks)

# Stop over compenstating, Limit to the actual spectral line
peak_magnitude_limits = peak_limits / ((1 / (np.sqrt(2 * np.pi * np.power(deviation, 2)))) *
       (np.power(np.e, -(np.power((0), 2) / (2 * np.power(deviation, 2))))))

line_limit = min(y)
print("Line-limit", line_limit)

t_x = np.linspace(x[0], x[-1], 1000)

popt, pcov = optimize.curve_fit(func, x, y, p0=([0] * len(peaks)) + [0.015], bounds=(([0] * len(peaks)) + [0], np.concatenate((peak_magnitude_limits, [line_limit]))))

print(popt)
t_y = func(t_x, *popt)

plt.figure()
plt.plot(x, y, label='Data', marker='o')
plt.plot(t_x, t_y, label='total')

for n in range(len(peaks)):
    t_y = gauss(t_x, peaks[n], deviation[n], popt[n])
    plt.plot(t_x, t_y, label=f'Peak {peaks[n] + 740} - {peak_guess[n]}')

t_y = line(t_x, popt[-1])
plt.plot(t_x, t_y, label=f'Background')

plt.legend()
plt.show()

"""
x = np.array(range(len(record["spectra"])))
y = np.array(record["spectra"])

trialX = np.linspace(x[0], x[-1], 1000)

fitted = np.polyfit(x, y, 100)[::-1]
t = np.zeros(len(trialX))
for i in range(len(fitted)):
    t += fitted[i] * trialX ** i

plt.figure()
plt.plot(x, y, label='Data', marker='o')
plt.plot(trialX, t, label='10 Deg Poly')
plt.legend()
plt.show()
"""

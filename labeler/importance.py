import matplotlib.pyplot as plt
from matplotlib import cm
import json
import numpy as np
from scipy import stats, optimize
import copy
import random
import openpyxl
from sklearn.metrics import r2_score

# using errors to give importance, means we can use 0 substance to act as null hypothesis
# nice water graph, shows not to give importance to exactly where the peak is (where the intersect is)


with open('substances.json') as f:
    data = json.load(f)

with open('substances_moieties.json') as f:
    data_moieties = json.load(f)

workbook = openpyxl.load_workbook('SCiO database v3.xlsx', data_only=True, read_only=True)
moieties_worksheet = workbook['moieties']

label_names = []

for f in range(8, 32):
    label_names.append(moieties_worksheet.cell(row=2, column=f).value)

targets = []
inputs = []
for record in data:
    # We're only looking at compounds in water
    if "water" not in record['volumes'] or len(record['volumes']) == 1:
        continue

    unknown_compound = False

    target = [0] * len(label_names)
    for compound in record['volumes']:
        if compound not in data_moieties:
            unknown_compound = True
            break

        for moiety in data_moieties[compound]:
            if moiety not in label_names:
                unknown_compound = True
                break


            index = label_names.index(moiety)
            target[index] += data_moieties[compound][moiety] * record['volumes'][compound]

    if unknown_compound:
        continue

    #inputs.append(np.gradient(record['spectra'][120:240]))
    inputs.append(np.gradient(record['spectra']))
    #inputs.append(record['spectra'])

    targets.append(target)

targets = np.array(targets)

empty_moities = np.where(~targets.any(axis=0))[0]

print(targets.shape)
targets = np.delete(targets, empty_moities, axis=1)
label_names = np.delete(label_names, empty_moities, axis=0)
print(targets.shape)



# Rather, lets map moities to amplitudes
model = []
errors = []
r2 = []
for n in range(len(inputs[0])):
    xtr = []
    ttr = []

    for f in range(len(inputs)):
        xtr.append(targets[f])
        ttr.append(inputs[f][n])

    coefs, resid = np.linalg.lstsq(xtr, ttr, rcond=None)[:2]

    model.append(coefs)

    pred = xtr @ coefs
    r2.append(r2_score(ttr, pred))
    #errors.append(resid)
    errors.append(np.std(ttr - pred))

# plt.title("$R^2$ score from least-squares algorithm")
# plt.xlabel("wavelength (nm)")
# plt.plot(range(740, 1071), r2)
# plt.show()

def generate_spectra(x, *argv):
    y = [0] * len(x)

    for i in range(len(x)):
        y[i] = argv @ model[i]

    return y


sample_index = np.random.randint(0, len(inputs))  # 40
sample = inputs[sample_index]
sample_target = targets[sample_index]

x = np.zeros(len(sample))
y = sample

popt, pcov = optimize.curve_fit(generate_spectra, x, y, p0=[0] * len(label_names), bounds=(0, np.Inf), sigma=errors, absolute_sigma=True)
# popt, pcov = optimize.curve_fit(generate_spectra, x, y, p0=[0] * len(label_names), bounds=(0, np.Inf))

"""
for i in [0, 6]:
    just_water = [0] * len(label_names)
    just_water[i] = 1
    just_water = np.array(just_water)

    plt.ylabel("Absorption $1^{st}$ derivative")
    plt.xlabel("Wavelength (nm)")

    plt.plot(range(740, 1071), generate_spectra(x, just_water), label=label_names[i])

plt.legend()
plt.show()

quit()
"""

fig, ax1 = plt.subplots()
ax1.plot(errors, color="k")

ax2 = ax1.twinx()
ax2.plot(y, color="r")
ax2.plot(generate_spectra(x, popt), color="b")

plt.show()

ticks = np.arange(len(label_names))
bar_width = 0.35
plt.bar(ticks + bar_width, sample_target, bar_width, color="r")
plt.bar(label_names, popt, bar_width, color="b")
plt.xticks(ticks, label_names)

plt.show()

"""
# TODO: we can transpose and do in one
for i in range(len(targets[0])):  # test
    errors = []
    for n in range(len(inputs[0])):  # test
        xtr = []
        ttr = []

        xte = []
        tte = []
        for f in range(len(inputs)):
            if targets[f][i] > 0:  # Don't like mess at 0... maybe take out?
                xtr.append(inputs[f][n])
                ttr.append(targets[f][i])

                # optional allow all data to be tested on?..
                xte.append(inputs[f][n])
                tte.append(targets[f][i])

        xtr = np.array(xtr)
        ttr = np.array(ttr)

        xte = np.array(xte)
        tte = np.array(tte)

        # We can't predict this moitie
        if len(xtr) == 0:
            break

        coefs, resid = np.linalg.lstsq(np.transpose([xtr]), ttr, rcond=None)[:2]

        pred = xte * coefs

        errors.append(r2_score(tte, pred))

    # We can't predict this moitie
    if len(errors) == 0:
        print(f"{label_names[i]} has no inputs")
        continue

    errors = np.array(errors)

    plt.title(f"{label_names[i]}")
    plt.plot(errors)
    plt.show()
"""

import matplotlib.pyplot as plt
from matplotlib import cm
import json
import numpy as np
from scipy import stats, optimize
import copy
import random

# cyclohexanethiol and heptanal are really similar.
# heptanol 0.1 = cyclohexanethiol 0.046 + heptanol 0.039
# diphenyl-ether = benzaldehyde
# linalool = acetone
# dipentene = methyl-acetate


# interesting interaction, seems to be around the same number, multiple
# wavelengths change the shape, not the overall number, suggests it is not addative.

with open('substances.json') as f:
    data = json.load(f)

data_preserved = copy.deepcopy(data)

for record in data:
    record['spectra'] = np.array(record['spectra'])

substance_models = {}


def find_random_substance(name):
    records = []
    for record in data_preserved:
        if name in record["volumes"]:
            records.append(record)

    return random.choice(records)


def generate_model(volumes, spectra):
    model = []

    for i in range(331):
        x = []
        y = []

        for f in range(len(volumes)):
            x.append(volumes[f])
            y.append(spectra[f][i])

        x = np.array(x)
        y = np.array(y)

        model.append(np.linalg.lstsq(np.transpose([x]), y, rcond=None)[0][0])

    return model


def generate_spectra(volumes):
    y = [0] * 331

    for name in volumes:
        if name not in substance_models:
            continue

        volume = volumes[name]

        for i in range(331):
            y[i] += substance_models[name][i] * volume

    return y


def reduce_substance(name):
    for record in data:
        if name not in record['volumes']:
            continue

        removed_substances = []
        for additive in record['volumes']:
            if additive == name:
                continue

            if additive not in substance_models:
                continue

            record['spectra'] -= generate_spectra({additive: record['volumes'][additive]})
            removed_substances.append(additive)

        for additive in removed_substances:
            del record['volumes'][additive]


def add_substance(name):
    reduce_substance(name)

    volumes = []
    spectra = []
    for i in range(len(data)):
        # allow through base water only! And then only isolated substances that had been in water before.
        # We're focusing on substances suspended in water
        if (len(data_preserved[i]['volumes']) == 1 and 'water' not in data_preserved[i]['volumes']) or len(data[i]['volumes']) != 1 or name not in data[i]['volumes']:
            continue

        volumes.append(data[i]['volumes'][name])
        spectra.append(data[i]['spectra'])

    if len(volumes) > 0:
        substance_models[name] = generate_model(volumes, spectra)


# Define list to be used
"""
substance_names = [
    'water', 'ethanol', 'acetone', 'citronellol', 'dipentene', 'carvone',
    'toluene', 'linalool', 'diisobutyl-ketone', 'tridec-1-ene', 'geraniol',
    'eugenol', 'methanol', 'nerolidol', 'heptanal', 'cyclohexanethiol', 'diphenyl-ether',
    'acetonitrile', 'methyl-acetate', 'cyclohexanone-diethylketal', '2-butyne',
    'ethylene-glycol', 'benzaldehyde', 'i-pentyl-acetate', 'cyclohexeneoxide',
    '1,4-cyclohexadiene', 'propane-1-2-diol', 'furan', 't-butanol', 't-butyl-methyl-ether',
    'alpha-angelica-lactone'
]
"""

substance_names = ['water']

for i in range(len(data)):
    if 'zz' not in "".join(data[i]['volumes'].keys()) and len(data[i]['volumes']) == 2 and 'water' in data[i]['volumes']:
        substance_names += data[i]['volumes'].keys()

substance_names = list(set(substance_names))
substance_names.insert(0, substance_names.pop(substance_names.index('water')))


for name in substance_names:
    add_substance(name)


def combinanator(x, *argv):
    volumes = {}
    for i in range(len(substance_names)):
        volumes[substance_names[i]] = argv[i]
    return generate_spectra(volumes)


unknown_sample = find_random_substance('water')

x = list(range(331))
y = unknown_sample['spectra']

popt, pcov = optimize.curve_fit(combinanator, x, y, p0=[0] * len(substance_names), bounds=(0, np.Inf))

print("Actual volumes:")
for name in sorted(unknown_sample['volumes'], key=unknown_sample['volumes'].get, reverse=True):
    print(f"{name:<30} {unknown_sample['volumes'][name]:>5.3f}")

print()

print("Predicted volumes:")
predicted_substaces = sorted(range(len(popt)), key=lambda k: popt[k], reverse=True)
for i in predicted_substaces:
    if popt[i] < 0.001:
        break

    print(f"{substance_names[i]:<30} {popt[i]:>5.3f}")

t_x = np.linspace(x[0], x[-1], len(x))
t_y = combinanator(t_x, *popt)

plt.plot(x, y, label='Real')
plt.plot(t_x, t_y, label='Estimate')

# for i in range(len(substance_names)):
for i in predicted_substaces:
    plt.plot(generate_spectra({substance_names[i]: popt[i]}), label=f'Estimate ({substance_names[i]})')

    if popt[i] < 0.001:
        break

plt.legend()
plt.show()

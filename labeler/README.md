**Note: I have left the collection json file, and the moieties spreadsheet.**

To setup:
```
python3 -m venv venv
source ./venv/bin/activate
pip install -r requirements.txt
```

To generate ```labeled.json```:
```
python main.py
```


http://www.winisi.com/NIRS_theory.htm

nir band assignment

Infrared spectroscopy correlation table

750 - 1080 nm
13333 - 9259 cm-1
13340 - 9250


temperatures will massively throw off readings?

import matplotlib.pyplot as plt
from matplotlib import cm
import openpyxl
import json
import numpy as np
from scipy import stats, optimize
import copy
import random

workbook = openpyxl.load_workbook('SCiO database v3.xlsx', data_only=True, read_only=True)
moieties_worksheet = workbook['moieties']

dataset = {}
label_names = []
for i in range(3, 133):  # moieties_worksheet.max_row):
    name = moieties_worksheet.cell(row=i, column=1).value

    if name is not None:
        name = name.lower()

        obj = {}
        try:
            rmm = float(moieties_worksheet.cell(row=i, column=5).value)
            density = float(moieties_worksheet.cell(row=i, column=6).value)
        except:
            continue

        for f in range(8, 32):
            moitie_name = moieties_worksheet.cell(row=2, column=f).value

            if moitie_name is not None:
                value = moieties_worksheet.cell(row=i, column=f).value
                if value is None:
                    value = 0

                obj[moitie_name] = float(value)
                obj[moitie_name] = obj[moitie_name] * density / (rmm * 1000)

        dataset[name] = obj


with open('substances_moieties.json', 'w') as f:
    json.dump(dataset, f)

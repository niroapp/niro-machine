import json
import openpyxl
import matplotlib.pyplot as plt
import numpy as np
import datetime

# NOTE: What do read collumns mean iwthin spreadsheet

# Load the json file from SCiO
with open('collection_id-87602aed-eb25-4221-a107-bf6b62cd5289-13644565226611702548.json') as f:
    data = json.load(f)

# Load the workbook
workbook = openpyxl.load_workbook('SCiO database v2.xlsx', read_only=True)
moieties_worksheet = workbook['moieties']

print(data["records"][0].keys())

# This is a slow process.. Maybe a different library?
moieties = {}
label_names = []
for i in range(3, 99):  # moieties_worksheet.max_row):
    name = moieties_worksheet.cell(row=i, column=1).value

    if name is not None:
        try:
            obj = {}
            obj["RMM"] = float(moieties_worksheet.cell(row=i, column=5).value)
            obj["density"] = float(moieties_worksheet.cell(row=i, column=6).value)
            obj["labels"] = {}
            for f in range(8, 32):
                obj["labels"][moieties_worksheet.cell(row=2, column=f).value] = float(moieties_worksheet.cell(row=i, column=f).value)
        except:
            continue

        # Save list of labels for later
        if len(label_names) == 0:
            label_names = obj["labels"].keys()

        moieties[name] = obj


# Generate a baseline of the apparatus

base_lines = {}
base_lines_count = {}

base_line = np.zeros(len(data["records"][0]["sample_raw"]))
base_line_count = 0

for record in data["records"]:
    if record["Name"] == "3-empty-vials":
        date = record["batch"]  # date = datetime.datetime.strptime(record["batch"], "%d/%m/%y")
        spectra = np.array([float(x) for x in record["sample_raw"]])

        if date not in base_lines:
            base_lines[date] = spectra
            base_lines_count[date] = 1
        else:
            base_lines[date] += spectra
            base_lines_count[date] += 1

        base_line += spectra
        base_line_count += 1

for key in base_lines.keys():
    base_lines[key] /= base_lines_count[key]

base_line /= base_line_count

dataset = []

# Empty vials left in training data as empty, still calibrated so pretty much zero
for record in data["records"]:
    name = record["Name"].lower()

    if name not in moieties:
        continue

    # if name == "3-empty-vials":
    #     continue

    spectra = None

    if record["batch"] not in base_lines:
        spectra = np.log(base_line / np.array([float(x) for x in record["sample_raw"]]))
    else:
        spectra = np.log(base_lines[record["batch"]] / np.array([float(x) for x in record["sample_raw"]]))

    # plt.plot(range(len(spectra)), spectra, 'g')
    # plt.show()
    # break

    volumes = {}
    volumes[name] = record["Sample Vol"]
    volumes["water"] = record["Water or SDS vol"]  # Note: can't be 'or'.. needs to be well defined

    moles = {}

    # labels = {"HOH": 0, "COH": 0}
    labels = {}

    for label_name in label_names:
        labels[label_name] = 0

    for key in volumes.keys():
        volumes[key] = 0 if volumes[key] == "n/a" else float(volumes[key])  # NOTE: just set to 0..

        if key not in moieties:
            print("Record does not reference a substance in worksheet.")
            continue

        moles[key] = volumes[key] * moieties[key]["density"] / (moieties[key]["RMM"] * 1000)

        for label_name in labels.keys():
            labels[label_name] += moles[key] * moieties[key]["labels"][label_name]

    dataset.append({"labels": labels, "spectra": spectra.tolist()})


with open('labeled.json', 'w') as f:
    json.dump(dataset, f)

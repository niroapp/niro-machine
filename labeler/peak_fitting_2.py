import matplotlib.pyplot as plt
from matplotlib import cm
import json
import numpy as np
from scipy import stats
import scipy.optimize as optimize

# Define peaks
peaks_labels = ["$H_2O$", "$CH$", "$CH_2$", "$CH_3$", "$ROH$", "$RNH_2$", "$ArCH$", "$ArOH$", "$ArCH$", "$H_2O$"]
peaks_lower_bounds = [937.53, 919.12, 897.06, 860.29, 910.29, 1016.17, 1082.35, 939.70, 864.71, 736.76]
peaks_upper_bounds = [978.62, 958.82, 939.71, 917.65, 951.47, 1055.88, 1110.00, 960.29, 883.82, 760.29]
peaks_deviation = [30, 8, 8, 8, 30, 15, 40, 10, 30, 30]


peaks_labels += ["$RNHR^1$", "$RNH_2$", "$CH$", "$CH_2$", "$CH_3$", "$ROH$", "$ArOH$", "$ArCH$"]
peaks_lower_bounds += [797.06, 767.65, 738.23, 719.11, 697.06, 719.12, 717.64, 700.0]
peaks_upper_bounds += [848.53, 826.47, 767.65, 755.88, 732.35, 738.24, 736.76, 726.47]
peaks_deviation += [30, 30, 30, 30, 30, 30, 30, 30]

# Load data
with open('labeled.json') as f:
    data = json.load(f)

# Pick a nice spectra for starters
record = data[48]

# Get spectra mappings wavelengths to magnitudes
x = np.array(range(len(record["spectra"]))) + 750
y = np.array(record["spectra"])


# Define functions for aproximation
def gauss(x, sigma, mu, magnitude):
    return magnitude * (
        (1 / (np.sqrt(2 * np.pi * np.power(sigma, 2)))) *
        (np.power(np.e, -(np.power((x - mu), 2) / (2 * np.power(sigma, 2)))))
    )


def func(x, *argv):
    result = 0
    for n in range(len(peaks_labels)):
        result += gauss(x, peaks_deviation[n], argv[n], argv[int((len(argv) / 2) + n)])

    return result


# Get the magn of each peak's wavelength
peaks_max_magnitudes = []

for i in range(len(peaks_lower_bounds)):
    magnitudes = y[(x >= peaks_lower_bounds[i]) & (x <= peaks_upper_bounds[i])]

    if len(magnitudes) == 0:  # If outside range, let it do anything..
        peaks_max_magnitudes.append(np.Inf)
    else:
        peaks_max_magnitudes.append(np.max(magnitudes))

# Stop over compenstating, Limit to the actual spectral line
peaks_magnitudes = peaks_max_magnitudes / (
    (1 / (np.sqrt(2 * np.pi * np.power(peaks_deviation, 2)))) *
    (np.power(np.e, -(np.power((0), 2) / (2 * np.power(peaks_deviation, 2)))))
)

lower_limits = np.concatenate((peaks_lower_bounds, [0] * len(peaks_labels)))
upper_limits = np.concatenate((peaks_upper_bounds, peaks_magnitudes))

print(lower_limits)
print(upper_limits)

t_x = np.linspace(x[0], x[-1], 1000)
popt, pcov = optimize.curve_fit(func, x, y, p0=lower_limits, bounds=(lower_limits, upper_limits))

print(popt)
t_y = func(t_x, *popt)

plt.figure()
plt.plot(x, y, label='Data', marker='o')
plt.plot(t_x, t_y, label='total')

for n in range(len(peaks_labels)):
    t_y = gauss(x, peaks_deviation[n], popt[n], popt[int((len(popt) / 2) + n)])
    plt.plot(x, t_y, label=f'Peak {int(popt[n])} ~ {peaks_labels[n]}')

#t_y = line(t_x, popt[-1])
#plt.plot(t_x, t_y, label=f'Background')

plt.legend()
plt.show()

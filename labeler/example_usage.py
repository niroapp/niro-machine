import matplotlib.pyplot as plt
import json
import numpy as np
from sklearn.naive_bayes import GaussianNB
from sklearn import metrics

# http://ogrisel.github.io/scikit-learn.org/sklearn-tutorial/tutorial/astronomy/classification.html

with open('labeled.json') as f:
    data = json.load(f)

x_train = []
t_train = []

for record in data:
    x_train.append(np.gradient(record["spectra"]))
    t_train.append(record["labels"]["COH"] > 0)

clf = GaussianNB()
clf.fit(x_train, t_train)

t_pred = clf.predict(x_train)
accuracy = float(np.sum(t_train == t_pred)) / len(t_train)
print(f"Accuracy: {accuracy}%")

print(metrics.classification_report(t_train, t_pred))

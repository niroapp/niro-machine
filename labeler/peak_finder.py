import matplotlib.pyplot as plt
from matplotlib import cm
import json
import numpy as np
from scipy import stats

with open('labeled.json') as f:
    data = json.load(f)


def show_spectra(label):
    Y = []
    c = []

    for record in data:
        #if record["labels"][label] == 0:
        #    continue

        #Y.append(np.gradient(np.gradient(record["spectra"])))
        Y.append(record["spectra"])
        c.append(record["labels"][label])

    c_max = max(c)
    c_min = min(c)

    for i in range(len(c)):
        plt.plot(Y[i], c=cm.spring((1 / (c_max - c_min)) * (c[i] - c_min)))

    plt.show()


def show_correlation(peak, label):
    x = []
    y = []

    for record in data:
        # if record["labels"][label] == 0:
        #     continue

        # x.append(record["spectra"][peak])
        x.append(np.gradient(record["spectra"])[peak])
        # x.append(np.gradient(np.gradient(record["spectra"]))[peak])
        y.append(record["labels"][label])

    slope, intercept, r_value, p_value, std_err = stats.linregress(x, y)
    abline_values = [slope * i + intercept for i in x]

    plt.plot(x, abline_values, 'b')
    plt.plot(x, y, "ro")
    plt.show()


def r_squared(peak, label):
    x = []
    y = []

    for record in data:
        # if record["labels"][label] == 0:
        #    continue

        # x.append(record["spectra"][peak])
        x.append(np.gradient(record["spectra"])[peak])
        # x.append(np.gradient(np.gradient(record["spectra"]))[peak])
        y.append(record["labels"][label])

    slope, intercept, r_value, p_value, std_err = stats.linregress(x, y)
    return r_value**2


def peak_finder(label):
    best_i = 0
    best_value = 0

    for i in range(330):
        value = r_squared(i, label)

        if best_value < value:
            best_value = value
            best_i = i

    print(f"For: {label}, Best wavelength: {best_i}, R^2: {best_value}")
    show_correlation(best_i, label)


# peaks
# HOH - 235
# CH2- 185
# CH3 - 166
# COH - 271
# COC - 132


# show_spectra("C=O")

# peak_finder("CH2") # 168
# peak_finder("CH3") # 158
# peak_finder("HOH") # 211
# peak_finder("COH") # 158, lol?
# peak_finder("COC") # 123
# peak_finder("C=O") # 146

"""
HOH - 213 eye, 212 best linear regression
COH - 160 eye, 153 best linear regression

I first eyeballed, and used one wavelength to linear regression. promissing
then calculated r^2 to figure out best wavelength in range, very similarself.
then tried with all data (before only ones that contained substance)
awful results, with false detecting when a substance is not there
trying 2nd deriviative
1st seems best.. added more samples

changes with temperature, even what type of water? (16/17)O H/D/T?
"""

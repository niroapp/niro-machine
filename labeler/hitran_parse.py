import matplotlib.pyplot as plt
import numpy as np

x = []
y = []

with open("./par/water.par") as f:
    for line in f:
        molecule_number = int(line[:2])
        isotype_number = int(line[2:3])
        transition_frequency = float(line[3:15])
        intensity = float(line[15:25])

        wavelength = (1/transition_frequency)*(10**7)

        print(wavelength, intensity)
        x.append(wavelength)
        y.append(intensity)

x = np.array(x).reshape(-1, 2).mean(axis=1)
y = np.array(y).reshape(-1, 2).mean(axis=1)

plt.plot(x, y)
plt.show()
